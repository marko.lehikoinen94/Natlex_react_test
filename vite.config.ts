import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  root:"./",
  build: {
    chunkSizeWarningLimit: 2000, // Adjust the chunk size warning limit as needed
    rollupOptions: {
      // You can customize chunking further if needed
      output: {
        manualChunks(id) {
          // Here, you can manually specify how to group modules into chunks
          // For example, you can group all React-related modules into a single chunk
          if (id.indexOf('node_modules/react') !== -1 || id.indexOf('node_modules/react-dom') !== -1) {
            return 'react-vendor';
          }
          // Return null for all other modules to use default chunking behavior
          return null;
        },
      },
    },
  },
  plugins: [react()],
  server: {
    port: 3000,
  },
});