import { Link, Route, BrowserRouter as Router, Routes } from "react-router-dom";

import Main from "./main";
import SettingsPage from "./settings";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Main />} />
      <Route path="/settings" element={<SettingsPage />} />
    </Routes>
  );
}

export default App;
