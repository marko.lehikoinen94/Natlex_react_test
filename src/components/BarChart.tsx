import {
  BarElement,
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  Title,
  Tooltip,
} from "chart.js";
import React, { useEffect, useState } from "react";

import { Bar } from "react-chartjs-2";
import dayjs from "dayjs";

/* 

*/

interface ChartData {
  id: string;
  chartName: string;
  chartType: string;
  chartColor: string;
}

interface AreaChartProps {
  chartData: ChartData;
  startDate: Date | null;
  endDate: Date | null;
}

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

// Main linechart functino
const BarChart: React.FC<AreaChartProps> = ({
  chartData,
  startDate,
  endDate,
}) => {
  const [labels, setLabels] = useState<string[]>([]);

  function generateRandomNumbers(
    min: number,
    max: number,
    count: number
  ): number[] {
    const randomNumbers: number[] = [];
    for (let i = 0; i < count; i++) {
      const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
      randomNumbers.push(randomNumber);
    }
    return randomNumbers;
  }

  // function that makes an array from start and end dates when they're set

  useEffect(() => {
    if (startDate && endDate) {
      const start = dayjs(startDate);
      const end = dayjs(endDate);
      const daysDiff = end.diff(start, "day") + 1;
      const newLabels = Array.from({ length: daysDiff }, (_, index) =>
        start.add(index, "day").format("DD/MM/YYYY")
      );
      setLabels(newLabels);
    }
  }, [startDate, endDate]);

  const data = {
    labels: labels,
    datasets: [
      {
        label: "Bar Chart",
        data: generateRandomNumbers(0, 150, labels.length), // Example data, replace with actual data
        borderColor: chartData.chartColor,
        backgroundColor: chartData.chartColor,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    aspectRatio: 2,
    height: "400px",
    scales: {
      y: {
        min: 0,
        max: 150,
      },
    },
  };

  return (
    <div
      style={{
        width: "auto",
        height: "14em",
        backgroundColor: "white",
        padding: "1em",
        borderRadius: "1em",
      }}
    >
      <Bar data={data} options={options} />
    </div>
  );
};

export default BarChart;
