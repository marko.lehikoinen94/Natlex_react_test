import { Button } from "@mui/material";
import { Settings } from "@mui/icons-material";

const SettingsButton = () => {
  return (
    <Button
      variant="contained"
      startIcon={<Settings />}
      sx={{
        height: "3em",
        width: "9rem",
        marginRight: "auto",
        bgcolor: "secondary.main",
        ml: "1em",
        "&:hover": {
          backgroundColor: "#1570e0",
        },
      }}
    >
      Settings
    </Button>
  );
};

export default SettingsButton;
