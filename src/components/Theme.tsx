import { ThemeProvider, createTheme } from "@mui/material";

const Theme = createTheme({
  palette: {
    primary: {
      main: "#181818",

      // light: will be calculated from palette.primary.main,
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      main: "#4c9988",
      light: "#F5EBFF",
      // dark: will be calculated from palette.secondary.main,
      contrastText: "#47008F",
    },
  },
  typography: {
    fontFamily: "Roboto",
    h1: {
      fontSize: "3rem",
      fontWeight: 400,
    },
    h2: {
      fontSize: "2rem",
      fontWeight: 400,
    },
    h3: {
      fontSize: "1.5rem",
      fontWeight: 400,
    },
  },

  components: {
    MuiTextField: {
      styleOverrides: {
        root: {
          color: "#ffffff",
          border: "0.5em solid",
          borderRadius: "0.5em",
          backgroundColor: "#ffffff",
        },
      },
    },
  },
});

export default Theme;
