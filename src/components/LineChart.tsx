import {
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LineElement,
  LinearScale,
  PointElement,
  Tooltip,
} from "chart.js";
import React, { useEffect, useState } from "react";

import { Line } from "react-chartjs-2";
import dayjs from "dayjs";

/* 

*/

interface ChartData {
  id: string;
  chartName: string;
  chartType: string;
  chartColor: string;
}

interface LineChartProps {
  chartData: ChartData;
  startDate: Date | null;
  endDate: Date | null;
}

ChartJS.register(
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
  Legend,
  Tooltip
);

// Main linechart function start and props:
const Linechart: React.FC<LineChartProps> = ({
  chartData,
  startDate,
  endDate,
}) => {
  // Constants here:
  const [labels, setLabels] = useState<string[]>([]);

  function generateRandomNumbers(
    min: number,
    max: number,
    count: number
  ): number[] {
    const randomNumbers: number[] = [];
    for (let i = 0; i < count; i++) {
      const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
      randomNumbers.push(randomNumber);
    }
    return randomNumbers;
  }

  // function that makes an array from start and end dates when they're set

  useEffect(() => {
    if (startDate && endDate) {
      const start = dayjs(startDate);
      const end = dayjs(endDate);
      const daysDiff = end.diff(start, "day") + 1;
      const newLabels = Array.from({ length: daysDiff }, (_, index) =>
        start.add(index, "day").format("DD/MM/YYYY")
      );
      setLabels(newLabels);
    }
  }, [startDate, endDate]);

  // console.log(labels.length);

  const data = {
    labels: labels,
    datasets: [
      {
        label: "Line chart",
        data: generateRandomNumbers(0, 100, labels.length), // Example data, replace with actual data
        borderColor: chartData.chartColor,
        pointBorderColor: "blue", // Example point color, you can change this if needed
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    aspectRatio: 2,
    height: "400px",
    scales: {
      y: {
        min: 0,
        max: 150,
      },
    },
  };

  return (
    <div
      style={{
        width: "auto",
        height: "14em",
        backgroundColor: "white",
        padding: "1em",
        borderRadius: "1em",
      }}
    >
      <Line data={data} options={options}></Line>
    </div>
  );
};

export default Linechart;
