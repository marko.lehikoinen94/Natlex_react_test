// types.ts

export interface ChartData {
    id: string;
    chartName: string;
    chartType: string;
    chartColor: string;
  }