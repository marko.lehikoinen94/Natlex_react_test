import React, { useEffect, useState } from "react";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import EditIcon from "@mui/icons-material/Edit";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import Modal from "@mui/material/Modal";
import SaveIcon from "@mui/icons-material/Save";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";

interface Props {
  updateData: (
    updatedChart: Partial<{
      chartName: string;
      chartType: string;
      chartColor: string;
    }>
  ) => void;
  existingData: {
    id: string;
    chartName: string;
    chartType: string;
    chartColor: string;
  };
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const btnstyle = {
  m: 2,
  backgroundColor: "rgb(0, 87, 68)",
  borderRadius: "1em",
  "&:hover": {
    backgroundColor: "rgb(9, 206, 0)",
  },
};

const ChartTypes = [
  {
    value: "line",
    label: "Line",
  },
  {
    value: "scatter",
    label: "Scatter",
  },
  {
    value: "area",
    label: "Area",
  },
  {
    value: "bar",
    label: "Bar",
  },
];

const ChartLineColors = [
  {
    value: "blue",
    label: "Blue",
  },
  {
    value: "red",
    label: "Red",
  },
  {
    value: "orange",
    label: "Orange",
  },
  {
    value: "purple",
    label: "Purple",
  },
];

/* 




*/

const ChartSettingsButton: React.FC<Props> = ({ updateData, existingData }) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  /* Enter chart name event change */
  const [chartName, setChartName] = useState<string>("");

  /* Chart type select change */
  const [chartType, setType] = useState("line");

  /* Chart color select change */
  const [chartColor, setColor] = useState("blue");

  /* 
  
  
  
  
  
  */
  const handleEdit = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    const { id } = existingData;
    const updatedChart = { chartName, chartType, chartColor };

    try {
      const response = await fetch(`http://localhost:8000/charts/${id}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(updatedChart),
      });
      if (!response.ok) {
        throw new Error("Failed to update chart");
      }
      console.log("Chart updated.");
      handleClose();
      updateData(updatedChart); // Pass updatedChart to updateData function
    } catch (error) {
      console.error("Error updating chart:", error);
    }
  };
  /* 




*/
  const handleChartNameChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setChartName(event.target.value);
  };
  /* 




*/
  return (
    <div>
      <IconButton onClick={handleOpen}>
        <EditIcon />
      </IconButton>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography variant="h3"> Modify chart </Typography>
          {/* chart name input form start*/}
          <Box
            component="form"
            sx={{
              "& > :not(style)": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="standard-basic"
              label="Enter new chart name:"
              variant="standard"
              color="secondary"
              onChange={handleChartNameChange}
            />
          </Box>
          {/* chart name input form end*/}

          {/* chart type select DROPDOWN start*/}
          <Box
            component="form"
            sx={{
              "& .MuiTextField-root": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <div>
              <TextField
                id="select-chart-type"
                select
                value={chartType}
                onChange={(e) => setType(e.target.value)}
                label="Type"
                defaultValue="line"
                helperText="Select chart type"
                variant="outlined"
                color="secondary"
              >
                {ChartTypes.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </div>
          </Box>
          {/* chart type select  DROPDOWN end*/}
          {/* chart colours select DROPDOWN start */}
          <Box
            component="form"
            sx={{
              "& .MuiTextField-root": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <div>
              <TextField
                id="select-chart-color"
                select
                value={chartColor}
                onChange={(e) => setColor(e.target.value)}
                label="Color"
                defaultValue="blue"
                helperText="Select chart color"
                variant="outlined"
                color="secondary"
              >
                {ChartLineColors.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </div>
            <Button
              onClick={handleEdit}
              sx={btnstyle}
              startIcon={<SaveIcon />}
              variant="contained"
              size="large"
            >
              SAVE
            </Button>
          </Box>

          {/* chart colours select DROPDOWN end */}
        </Box>
      </Modal>
    </div>
  );
};

export default ChartSettingsButton;
