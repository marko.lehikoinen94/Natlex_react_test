import { Box, Paper, Typography } from "@mui/material";

import ChartSettingsButton from "./ChartSettingsButton";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import React from "react";

interface ChartData {
  id: string;
  chartName: string;
  chartType: string;
  chartColor: string;
}

interface ChartBoxProps {
  chartData: ChartData;
  onDelete: () => void;
  updateData: (updatedChart: Partial<ChartData>) => void;
}
/* 





*/
const ChartBox: React.FC<ChartBoxProps> = ({
  chartData,
  onDelete,
  updateData,
}) => {
  const { chartName, chartType, chartColor } = chartData;

  return (
    <Paper elevation={8} sx={{ m: "2em", borderRadius: "1em" }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "right",
        }}
      >
        <ChartSettingsButton
          updateData={updateData}
          existingData={chartData} // Pass chartData as existingData
        />

        <IconButton aria-label="delete" onClick={onDelete}>
          <DeleteIcon />
        </IconButton>
      </Box>
      <Box sx={{ mx: 3, mb: 4 }}>
        <Typography variant="h3" sx={{ textAlign: "center" }}>
          {chartName}
        </Typography>

        <Typography sx={{ my: 2 }}>Chart type: {chartType}</Typography>
        <Typography sx={{ my: 2 }}>Chart color: {chartColor}</Typography>
      </Box>
    </Paper>
  );
};

export default ChartBox;
