import { Box, Button } from "@mui/material";

import HomeIcon from "@mui/icons-material/Home";
import { Link } from "react-router-dom";

const style = {
  bgcolor: "secondary.main",
  ml: "1em",
  "&:hover": {
    backgroundColor: "#1570e0",
  },
};

const Homebutton = () => {
  return (
    <Box
      sx={{
        marginLeft: "auto",
        display: "flex",
        alignItems: "center",
      }}
    >
      <Link to="/">
        <Button
          variant="contained"
          startIcon={<HomeIcon />}
          size="large"
          sx={style}
        >
          Home
        </Button>
      </Link>
    </Box>
  );
};

export default Homebutton;
