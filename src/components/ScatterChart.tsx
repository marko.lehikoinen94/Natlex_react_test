import {
  Chart as ChartJS,
  Legend,
  LineElement,
  LinearScale,
  PointElement,
  Tooltip,
} from "chart.js";
import React, { useEffect, useState } from "react";

import { Scatter } from "react-chartjs-2";
import dayjs from "dayjs";
import { faker } from "@faker-js/faker";

/* 

*/

interface ChartData {
  id: string;
  chartName: string;
  chartType: string;
  chartColor: string;
}

interface AreaChartProps {
  chartData: ChartData;
  startDate: Date | null;
  endDate: Date | null;
}

ChartJS.register(LinearScale, PointElement, LineElement, Tooltip, Legend);

// Main linechart functino
const ScatterChart: React.FC<AreaChartProps> = ({
  chartData,
  startDate,
  endDate,
}) => {
  // Constants here:
  const [labels, setLabels] = useState<string[]>([]);

  function generateRandomNumbers(
    min: number,
    max: number,
    count: number
  ): number[] {
    const randomNumbers: number[] = [];
    for (let i = 0; i < count; i++) {
      const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
      randomNumbers.push(randomNumber);
    }
    return randomNumbers;
  }

  // function that makes an array from start and end dates when they're set

  useEffect(() => {
    if (startDate && endDate) {
      const start = dayjs(startDate);
      const end = dayjs(endDate);
      const daysDiff = end.diff(start, "day") + 1;
      const newLabels = Array.from({ length: daysDiff }, (_, index) =>
        start.add(index, "day").format("DD/MM/YYYY")
      );
      setLabels(newLabels);
    }
  }, [startDate, endDate]);

  const data = {
    datasets: [
      {
        label: "Scatter chart",
        data: Array.from({ length: labels.length }, () => ({
          x: faker.number.int({ min: 1, max: 140 }),
          y: faker.number.int({ min: 1, max: 140 }),
        })),
        backgroundColor: chartData.chartColor,
      },
    ],
  };

  const options = {
    radius: 5,
    pointStyle: "circle",
    responsive: true,
    maintainAspectRatio: false,
    aspectRatio: 2,
    height: "400px",
    scales: {
      y: {
        min: 0,
        max: 150,
      },
      x: {
        min: dayjs(startDate).get("date"),
        max: dayjs(endDate).get("date"),
      },
    },
  };

  return (
    <div
      style={{
        width: "auto",
        height: "14em",
        backgroundColor: "white",
        padding: "1em",
        borderRadius: "1em",
      }}
    >
      <Scatter data={data} options={options}></Scatter>
    </div>
  );
};

export default ScatterChart;
