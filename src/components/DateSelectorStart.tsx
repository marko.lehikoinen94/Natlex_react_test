import "dayjs/locale/en-gb";

import dayjs, { Dayjs } from "dayjs";

import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { useState } from "react";

interface StartDateSelectorProps {
  onStartDateChange: (newStartDate: Date | null) => void;
}

function StartDateSelector({ onStartDateChange }: StartDateSelectorProps) {
  const [selectedDate, setSelectedDate] = useState<Dayjs | null>(null);

  const handleDateChange = (newValue: Dayjs | null) => {
    setSelectedDate(newValue);
    onStartDateChange(newValue ? newValue.toDate() : null);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="en-gb">
      <DatePicker
        label="Start date"
        value={selectedDate}
        onChange={handleDateChange}
        slotProps={{
          textField: { size: "small" },
        }}
        sx={{
          marginLeft: "auto",
          display: "flex",
          mx: { xs: 0, md: 0 },
        }}
      ></DatePicker>
    </LocalizationProvider>
  );
}

export default StartDateSelector;
