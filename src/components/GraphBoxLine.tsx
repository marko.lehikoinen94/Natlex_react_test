import { Box, Paper, Typography } from "@mui/material";

import { ChartData } from "./types";
import Linechart from "./LineChart";
import React from "react";

interface ServiceBoxProps {
  service: string;
  chartData: ChartData[];
  startDate: Date | null;
  endDate: Date | null;
}

const GraphBoxLine: React.FC<ServiceBoxProps> = ({
  service,
  chartData,
  startDate,
  endDate,
}) => {
  const filteredChartData = chartData.filter(
    (chart) => chart.chartType === "line" && chart.chartName === service
  );

  if (filteredChartData.length === 0) {
    return null; // If no data for this chart, don't render anything
  }
  return (
    <Paper
      elevation={8}
      sx={{
        width: { xs: "100%", md: "40%" },
        height: { xs: "40%", md: "auto" },
      }}
    >
      <Box sx={{}}>
        <Typography variant="h3" sx={{ textAlign: "center", my: "0.5em" }}>
          {service}
        </Typography>
        <Linechart
          chartData={filteredChartData[0]}
          startDate={startDate}
          endDate={endDate}
        />
      </Box>
    </Paper>
  );
};

export default GraphBoxLine;
