import { Box, Paper, Typography } from "@mui/material";

import BarChart from "./BarChart";
import { ChartData } from "./types";
import React from "react";
import SplineChart from "./ScatterChart";

interface ServiceBoxProps {
  service: string;
  chartData: ChartData[];
  startDate: Date | null;
  endDate: Date | null;
}

const GraphBoxScatter: React.FC<ServiceBoxProps> = ({
  service,
  chartData,
  startDate,
  endDate,
}) => {
  const filteredChartData = chartData.filter(
    (chart) => chart.chartType === "scatter" && chart.chartName === service
  );

  if (filteredChartData.length === 0) {
    return null; // If no data for this chart, don't render anything
  }
  return (
    <Paper
      elevation={8}
      sx={{
        width: { xs: "100%", md: "40%" },
        height: { xs: "40%", md: "auto" },
      }}
    >
      <Box>
        <Typography variant="h3" sx={{ textAlign: "center", my: "0.5em" }}>
          {service}
        </Typography>
        <SplineChart
          chartData={filteredChartData[0]}
          startDate={startDate}
          endDate={endDate}
        />
      </Box>
    </Paper>
  );
};

export default GraphBoxScatter;
