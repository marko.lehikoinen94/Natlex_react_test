import {
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Typography,
} from "@mui/material";

import { useState } from "react";

const MuiCheckbox = () => {
  const [graphs, setGraphs] = useState<string[]>([]);
  console.log({ graphs });

  const handleGraphChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const index = graphs.indexOf(event.target.value);
    if (index === -1) {
      setGraphs([...graphs, event.target.value]);
    } else {
      setGraphs(graphs.filter((graph) => graph !== event.target.value));
    }
  };
  return (
    <Box>
      <Typography
        sx={{
          fontSize: "2em",
          textAlign: "center",
          color: "rgb(255, 255, 255)",
          textShadow: "2px 2px 6px black",
        }}
      >
        Select wanted graphs to show:
      </Typography>
      <Box>
        <FormControl>
          <FormLabel>Graphs</FormLabel>
          <FormGroup>
            <FormControlLabel
              label="Line"
              value="line"
              control={
                <Checkbox
                  checked={graphs.includes("line")}
                  onChange={handleGraphChange}
                />
              }
            />
            <FormControlLabel
              label="Bar"
              value="bar"
              control={
                <Checkbox
                  checked={graphs.includes("bar")}
                  onChange={handleGraphChange}
                />
              }
            />
            <FormControlLabel
              label="Area"
              value="area"
              control={
                <Checkbox
                  checked={graphs.includes("area")}
                  onChange={handleGraphChange}
                />
              }
            />
            <FormControlLabel
              label="Band"
              value="band"
              control={
                <Checkbox
                  checked={graphs.includes("band")}
                  onChange={handleGraphChange}
                />
              }
            />
          </FormGroup>
        </FormControl>
      </Box>
    </Box>
  );
};

export default MuiCheckbox;
