import React from "react";
import { Typography } from "@mui/material";

const SettingsHeader: React.FC = () => {
  return (
    <Typography
      variant="h1"
      sx={{
        padding: "0.5em",
        color: "secondary.main",
      }}
    >
      Settings
    </Typography>
  );
};

export default SettingsHeader;
