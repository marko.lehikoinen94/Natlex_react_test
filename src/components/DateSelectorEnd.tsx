import "dayjs/locale/en-gb";

import dayjs, { Dayjs } from "dayjs";

import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFnsV3";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { Box } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { useState } from "react";

interface StartDateSelectorProps {
  onEndDateChange: (newEndDate: Date | null) => void;
}

function EndDateSelector({ onEndDateChange }: StartDateSelectorProps) {
  const [selectedDate, setSelectedDate] = useState<Dayjs | null>(null);

  const handleDateChange = (newValue: Dayjs | null) => {
    setSelectedDate(newValue);
    onEndDateChange(newValue ? newValue.toDate() : null);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale="en-gb">
      <DatePicker
        label="End date"
        value={selectedDate}
        onChange={handleDateChange}
        slotProps={{
          textField: { size: "small" },
        }}
        sx={{ marginLeft: { xs: "1em", md: "1em" } }}
      />
    </LocalizationProvider>
  );
}

export default EndDateSelector;
