import { Box, Typography } from "@mui/material";

import React from "react";

const Header: React.FC = () => {
  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <Typography
        sx={{
          fontSize: { xs: "2em", md: "3em" },
          padding: "0.5em",
          color: "secondary.main",
          flexGrow: 3,
        }}
      >
        View mode
      </Typography>
    </Box>
  );
};

export default Header;
