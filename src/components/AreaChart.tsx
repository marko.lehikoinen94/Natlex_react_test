import {
  CategoryScale,
  Chart as ChartJS,
  Filler,
  Legend,
  LineElement,
  LinearScale,
  PointElement,
  Tooltip,
} from "chart.js";
import React, { useEffect, useState } from "react";

import { Line } from "react-chartjs-2";
import dayjs from "dayjs";

interface ChartData {
  id: string;
  chartName: string;
  chartType: string;
  chartColor: string;
}

interface AreaChartProps {
  chartData: ChartData;
  startDate: Date | null;
  endDate: Date | null;
}

ChartJS.register(
  Filler,
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
  Legend,
  Tooltip
);

const AreaChart: React.FC<AreaChartProps> = ({
  chartData,
  startDate,
  endDate,
}) => {
  const [labels, setLabels] = useState<string[]>([]);

  function generateRandomNumbers(
    min: number,
    max: number,
    count: number
  ): number[] {
    const randomNumbers: number[] = [];
    for (let i = 0; i < count; i++) {
      const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
      randomNumbers.push(randomNumber);
    }
    return randomNumbers;
  }

  // function that makes an array from start and end dates when they're set

  useEffect(() => {
    if (startDate && endDate) {
      const start = dayjs(startDate);
      const end = dayjs(endDate);
      const daysDiff = end.diff(start, "day") + 1;
      const newLabels = Array.from({ length: daysDiff }, (_, index) =>
        start.add(index, "day").format("DD/MM/YYYY")
      );
      setLabels(newLabels);
    }
  }, [startDate, endDate]);

  const data = {
    labels: labels,
    datasets: [
      {
        label: "Area chart",
        data: generateRandomNumbers(0, 100, labels.length), // Example data, replace with actual data
        borderColor: chartData.chartColor,
        pointBorderColor: "blue", // Example point color, you can change this if needed
        tension: 0.4, // Tension based on your JSON data or keep it constant
        fill: true,
        backgroundColor: `rgba(76, 157, 213, 0.4)`,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    aspectRatio: 2,
    height: "400px",
    scales: {
      y: {
        min: 0,
        max: 150,
      },
    },
  };

  return (
    <div
      style={{
        width: "auto",
        height: "14em",
        backgroundColor: "white",
        padding: "1em",
        borderRadius: "1em",
      }}
    >
      <Line data={data} options={options}></Line>
    </div>
  );
};

export default AreaChart;
