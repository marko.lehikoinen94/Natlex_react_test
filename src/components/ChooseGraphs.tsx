import React, { useEffect, useState } from "react";
import { useFetcher, useNavigate } from "react-router-dom";

import AddChartButton from "./AddChartButton";
import { Box } from "@mui/material";
import ChartBox from "./ChartBox";

interface ChartData {
  id: string;
  chartName: string;
  chartType: string;
  chartColor: string;
}
/* 








*/
function ChooseGraphs() {
  const [chartData, setChartData] = useState<ChartData[]>([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    fetch("http://localhost:8000/charts")
      .then((res) => res.json())
      .then((data) => {
        navigate("/settings");
        setChartData(data);
      })
      .catch((error) => console.error("Error fetching data:", error));
  };
  /* 




*/
  const handleDelete = (id: string) => {
    fetch(`http://localhost:8000/charts/${id}`, {
      method: "DELETE",
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error("Failed to delete chart");
        }
        return res.json();
      })
      .then((data) => {
        console.log("Chart deleted:", data);
        // Update state
        setChartData((prevChartData) =>
          prevChartData.filter((chart) => chart.id !== id)
        );
      })
      .catch((error) => console.error("Error deleting chart:", error));
  };

  /* 




*/

  const updateData = (updatedChart: Partial<ChartData>) => {
    setChartData((prevChartData) =>
      prevChartData.map((chart) =>
        chart.id === updatedChart.id ? { ...chart, ...updatedChart } : chart
      )
    );
    fetchData();
    console.log("updating list...");
  };

  /* 



*/

  return (
    <Box
      sx={{
        display: "block",
        bgcolor: "rgb(76, 153, 136)",
        mx: "auto",
        mt: "1em",
        mb: "2em",

        borderRadius: "1em",
      }}
    >
      {/* Add chartbutton box */}
      <Box
        sx={{
          display: "flex",
          justifyContent: "right",
        }}
      >
        <AddChartButton fetchData={fetchData} />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: { xs: "column", md: "row" },
          justifyContent: "space-evenly",
          gap: 2,
        }}
      >
        {chartData.map((chart) => (
          <ChartBox
            key={chart.id}
            chartData={chart}
            onDelete={() => handleDelete(chart.id)}
            updateData={updateData}
          />
        ))}
      </Box>
    </Box>
  );
}

export default ChooseGraphs;
