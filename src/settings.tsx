import { Box, Button, Container } from "@mui/material";

import ChooseGraphs from "./components/ChooseGraphs";
import Homebutton from "./components/Homebutton";
import SettingsHeader from "./components/SettingsHeader";

const SettingsPage = () => {
  return (
    <Container maxWidth={false}>
      <Box>
        {/* box header + homebutton start */}
        <Box
          sx={{
            display: "flex",
          }}
        >
          <SettingsHeader />
          <Homebutton />
        </Box>
        {/* box header + homebutton end */}
        {/* container with Mui */}
        <ChooseGraphs />
      </Box>
    </Container>
  );
};

export default SettingsPage;
