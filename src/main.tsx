import { Box, Container, Typography } from "@mui/material";
import { BrowserRouter, Link } from "react-router-dom";
import { useEffect, useState } from "react";

import App from "./App";
import EndDateSelector from "./components/DateSelectorEnd";
import GraphBoxArea from "./components/GraphBoxArea";
import GraphBoxBar from "./components/GraphBoxBar";
import GraphBoxLine from "./components/GraphBoxLine";
import GraphBoxScatter from "./components/GraphBoxScatter";
import Header from "./components/Header";
import ReactDOM from "react-dom/client";
import SettingsButton from "./components/Settingsbutton";
import StartDateSelector from "./components/DateSelectorStart";
import Theme from "./components/Theme";
import { ThemeProvider } from "@mui/material";

const ContainerStyle = {
  bgcolor: "#1f1f1f",
  height: "100em",
  width: "100%",
  display: "block",
  marginbottom: "2em",
};

interface ChartData {
  id: string;
  chartName: string;
  chartType: string;
  chartColor: string;
}

//local styles etc.

function Main() {
  // State to hold chart data
  const [chartData, setChartData] = useState<ChartData[]>([]);

  const [selectedStartDate, setSelectedStartDate] = useState<Date | null>(null);
  const [selectedEndDate, setSelectedEndDate] = useState<Date | null>(null);

  const handleEndDateChange = (newEndDate: Date | null) => {
    setSelectedEndDate(newEndDate);
  };

  const handleStartDateChange = (newStartDate: Date | null) => {
    setSelectedStartDate(newStartDate);
  };

  // load data using useEffect
  useEffect(() => {
    fetchData();
  }, []);

  // fetching the data
  const fetchData = () => {
    fetch("http://localhost:8000/charts")
      .then((res) => res.json())
      .then((data) => {
        setChartData(data);
        // console.log(data);
      })
      .catch((error) => console.error("Error fetching data:", error));
  };

  // Grouping chart data by chart type
  const chartDataByType: { [key: string]: ChartData[] } = {};
  chartData.forEach((chart) => {
    if (!chartDataByType[chart.chartType]) {
      chartDataByType[chart.chartType] = [];
    }
    chartDataByType[chart.chartType].push(chart);
  });

  return (
    /* MAIN CONTAINER START */
    <Container maxWidth={false} sx={ContainerStyle}>
      {/* Header, Date and button section start */}
      <Box
        sx={{
          marginLeft: "",
          display: "flex",
          paddingBottom: "1em",
          alignItems: "center",

          // justifyContent: "space-around",
        }}
      >
        <Header />

        <Box
          sx={{
            marginLeft: "auto",
            display: { xs: "none", md: "flex" },
            alignItems: "center",
            // justifyContent: "space-around",
          }}
        >
          <StartDateSelector onStartDateChange={handleStartDateChange} />
          <EndDateSelector onEndDateChange={handleEndDateChange} />
        </Box>
        <Box
          sx={{
            marginLeft: { xs: "auto", md: 0 },
          }}
        >
          <Link to="/settings">
            <SettingsButton />
          </Link>
        </Box>

        {/* Button as link to settings => */}
      </Box>

      <Box
        sx={{
          // marginLeft: "auto",
          alignItems: "center",
          display: { xs: "flex", md: "none" },
          paddingBottom: "2em",

          justifyContent: "center",
        }}
      >
        <StartDateSelector onStartDateChange={handleStartDateChange} />
        <EndDateSelector onEndDateChange={handleEndDateChange} />
      </Box>

      {/* Header, Date and button section end */}

      {/* datawindows for the graphs START */}
      {selectedStartDate && selectedEndDate ? (
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: { xs: "column", md: "row" },
            justifyContent: "space-evenly",
            gap: 4,
          }}
        >
          {Object.keys(chartDataByType).map((type) => {
            switch (type) {
              case "line":
                return chartDataByType[type].map((chart, index) => (
                  <GraphBoxLine
                    key={index}
                    service={chart.chartName}
                    chartData={chartData}
                    startDate={selectedStartDate}
                    endDate={selectedEndDate}
                  />
                ));
              case "scatter":
                return chartDataByType[type].map((chart, index) => (
                  <GraphBoxScatter
                    key={index}
                    service={chart.chartName}
                    chartData={chartData}
                    startDate={selectedStartDate}
                    endDate={selectedEndDate}
                  />
                ));
              case "area":
                return chartDataByType[type].map((chart, index) => (
                  <GraphBoxArea
                    key={index}
                    service={chart.chartName}
                    chartData={chartData}
                    startDate={selectedStartDate}
                    endDate={selectedEndDate}
                  />
                ));
              case "bar":
                return chartDataByType[type].map((chart, index) => (
                  <GraphBoxBar
                    key={index}
                    service={chart.chartName}
                    chartData={chartData}
                    startDate={selectedStartDate}
                    endDate={selectedEndDate}
                  />
                ));
              default:
                return null;
            }
          })}
        </Box>
      ) : (
        <Box
          sx={{
            mx: "3em",
            padding: "2em",
            borderRadius: "2em",
            backgroundColor: "secondary.main",
          }}
        >
          <Typography
            variant="body1"
            align="center"
            sx={{
              fontSize: { xs: "2em", md: "3em" },
              color: "whitesmoke",
              textShadow: "1px 4px 10px black",
            }}
          >
            Please select start and end dates to view the charts.
          </Typography>
        </Box>
      )}
    </Container>
    /* MAIN CONTAINER END */
  );
}

const Root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

Root.render(
  <BrowserRouter>
    <ThemeProvider theme={Theme}>
      <App />
    </ThemeProvider>
  </BrowserRouter>
);

export default Main;
